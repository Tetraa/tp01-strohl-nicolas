$(function () {

    $("#form").on("submit", function (event) {
        if(!checkIfInputsAreValid())
        {
            event.preventDefault();
        }
        else
        {
            $("#form").trigger("submit");
        }
    })

})

function checkIfInputsAreValid()
{
    if($("#password").val() !== $("#password2").val())
    {
        showError("Les mots de passe ne correspondent pas.");
    }
    else
    {
        return true;
    }
    return false;
}

function showError(error)
{
    $("#errorMessage").html(error);
}